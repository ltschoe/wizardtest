<?php

$headers = [
    'Content-Type' => 'application/x-www-form-urlencoded',
    'Accept' => 'application/json',
];

$data=SdkRestApi::getParam('params');

$client = new \GuzzleHttp\Client([
    'headers' => $headers, 'verify' => false
]);

$res = $client->post('https://account.datawow.de/collect/api/account/new', [
    'form_params' => [
      'mail' => $data["u_mail"],
      'password' => 'qwertzuiop',
      'name' => $data["u_contactPerson"],
      'companyName' => $data["u_company"],
      'phone' => $data["u_phone"],
      'address' => $data["u_street"],
      'city' => $data["u_city"],
      'zip' => $data["u_zipcode"],
      'hash' => '0F6A4C18A870F9CCCD87AC0222890132106B699179FCBCA8E8A1B3C5B5282368'
    ]
]);

$res1 = $client->post('https://account.datawow.de/collect/api/plenty/new',
                      [
                        'auth'=>[$data["u_mail"],'qwertzuiop']
                      ,

                        'form_params' => [
                          'user' => $data["u_restUser"],
                          'password' => $data["u_restPW"],
                          'shopUri' => $data["u_shopUrl"],
                          'payload' => $data["u_annularSales"],
                          'hash' => '0F6A4C18A870F9CCCD87AC0222890132106B699179FCBCA8E8A1B3C5B5282368'
                          ]
                      ]
                    );


/** @return array */
return json_decode($res->getBody(), true);
