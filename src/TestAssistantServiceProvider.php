<?php

namespace Wizardo;

use Plenty\Plugin\ServiceProvider;
use Plenty\Modules\Wizard\Contracts\WizardContainerContract;
use Wizardo\Wizards\TestAssistant;

/**
 * Class dataWowServiceProvider
 */
class TestAssistantServiceProvider extends ServiceProvider
{
    /**
     * @return void
     */
	public function register()
	{

	}


	public function boot( WizardContainerContract $wizardContainerContract)
	{
	    $wizardContainerContract->register('test1812', TestAssistant::class);

	}
}
