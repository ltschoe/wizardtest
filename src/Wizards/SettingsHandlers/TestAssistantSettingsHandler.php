<?php

namespace Wizardo\Wizards\SettingsHandlers;


use Wizardo\Wizards\TestAssistant;
use Plenty\Modules\Wizard\Contracts\WizardSettingsHandler;
use Plenty\Modules\Plugin\Libs\Contracts\LibraryCallContract;
 use Plenty\Plugin\Http\Request;

use Plenty\Plugin\Log\Loggable;
use Plenty\Plugin\Log\Reportable;


class TestAssistantSettingsHandler implements WizardSettingsHandler
{
    use Loggable;
    use Reportable;
    /**
     * Handle wizard data for a finalized wizard
     *
     * @param array $parameters
     * @return bool
     */
    public function handle(array $data): bool
    {
      $libCall=pluginApp(LibraryCallContract::class);

      $parameters=$data["data"];

      $this->getLogger('plugin')
            //->addReference('mail',$parameters["u_mail"])
            ->alert('Mail',$parameters["u_mail"],$data["u_mail"]);

      //$this->report('reporter', 'WIZARDO', ['userId' => $parameters["u_mail"] ], ['usermial' => $parameters->u_mail]);

      $packagistResult =
			$libCall->call(
				'Wizardo::client0',
        ['params'=>$parameters]
			);

      $this->getLogger('plugin')
            //->addReference('mail',$parameters["u_mail"])
            ->alert('guzzle',$packagistResult );

      return true;

    }

}
