<?php

namespace Wizardo\Wizards;

use Wizardo\Wizards\SettingsHandlers\TestAssistantSettingsHandler;
use Plenty\Modules\Wizard\Services\WizardProvider;
use Plenty\Plugin\Application;

class TestAssistant extends WizardProvider
{

    protected function structure(): array
    {
        return [
            'key' => 'testWizard',
            'title' => 'wizard.title',
            'shortDescription' => 'wizard.shortDescription',
            'translationNamespace' => 'Wizardo',
            'topics' => ['integration'],
            'iconPath' => $this->getIcon(),
            'settingsHandlerClass' => 'Wizardo\Wizards\SettingsHandlers\TestAssistantSettingsHandler',
            'steps' => [
                'step1' => [
                    'title' => 'wizard.companyInformationTitle',
                    'description' => 'wizard.companyInformationDescription',
                    "sections" => [
                        [
                            "title" => "wizard.companyInformationTitlesectionTitle",
                            "form" => [
                                "u_company" => [
                                    "type" => "text",
                                    "options" => [
                                        "name" => "wizard.u_company"
                                    ]
                                ],
                                "u_ceo" => [
                                    "type" => "text",
                                    "options" => [
                                        "name" => "wizard.u_ceo"
                                    ]
                                ],
                                "u_street" => [
                                    "type" => "text",
                                    "options" => [
                                        "name" => "wizard.u_street"
                                    ]
                                 ],
                                "u_zipcode" => [
                                    "type" => "text",
                                    "options" => [
                                        "name" => "wizard.u_zipcode"
                                    ]
                                ],
                                "u_city" => [
                                    "type" => "text",
                                    "options" => [
                                        "name" => "wizard.u_city"
                                    ]
                                ],
                                "u_annularSales" => [
                                    "type" => "select",
                                    "options" => [
                                        "name" => "wizard.u_annularSales",
                                        "listBoxValues" => [
                                            [
                                                "caption" => "<0.5 Mio",
                                                "value" => 1
                                            ],
                                            [
                                                "caption" => "<1.5 Mio",
                                                "value" => 2
                                            ],
                                            [
                                                "caption" => "<2.5 Mio",
                                                "value" => 3
                                            ]
                                            ,
                                            [
                                                "caption" => ">2.5 Mio",
                                                "value" => 4
                                            ]
                                        ]
                                    ]
                                ],
                            ]

                        ],
                    ],
                ],
                'step2' => [

                    'title' => 'wizard.contactPersonTitle',
                    'description' => 'wizard.contactPersonDescription',
                    "sections" => [
                        [
                            "title" => "wizard.contactPersonSectionTitle",
                            "description"=>"wizard.contactPersonSectionDescription",
                            "form" => [
                                "u_contactPerson" => [
                                    "type" => "text",
                                    "options" => [
                                        "name" => "wizard.u_contactPerson"
                                    ]
                                ],
                                "u_mail" => [
                                    "type" => "text",
                                    "options" => [
                                        "name" => "wizard.u_mail"
                                    ]
                                ],
                                "u_phone" => [
                                    "type" => "text",
                                    "options" => [
                                        "name" => "wizard.u_phone"
                                    ]
                                ],
                            ]
                        ],
                ]
            ],
                'step3' => [

                    'title' => 'wizard.restUserTitle',
                    'description' => 'wizard.restUserDescription',
                    "sections" => [
                        [
                            "title" => "wizard.restUserSectionTitle",
                            "description"=>"wizard.restUserSectionDescription",
                            "form" => [
                                "u_shopUrl" => [
                                    "type" => "text",
                                    "options" => [
                                        "name" => "wizard.u_shopUrl"
                                    ]
                                ],
                                "u_restUser" => [
                                    "type" => "text",
                                    "options" => [
                                        "name" => "wizard.u_restUser"
                                    ]
                                ],
                                "u_restPW" => [
                                    "type" => "text",
                                    "options" => [
                                        "name" => "wizard.u_restPW"
                                    ]
                                ],
                            ]
                        ],
                    ]
                ]
        ]];
    }

    public function getIcon()
    {
        $path = '/images/logo-datawow-de.png';
        /** @var Application $app */
        $app = pluginApp(Application::class);
        $icon = $app->getUrlPath('Wizardo');

        $iconPath = $icon . $path;

        return $iconPath;
    }
}
